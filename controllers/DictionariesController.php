<?php

namespace app\controllers;

use app\models\Dictionaries;
use app\models\enums\DictionaryType;
use app\models\search\DictionariesSearch;
use Yii;
use yii\filters\VerbFilter;
use yii\web\Controller;

/**
 * CountriesController implements the CRUD actions for Countries model.
 */
class DictionariesController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Countries models.
     * @return mixed
     */
    public function actionIndex($type = DictionaryType::COUNTRY)
    {
        $searchModel = new DictionariesSearch();
        $dataProvider = $searchModel->search($type, Yii::$app->request->queryParams);

        return $this->render('index', [
            'type' => $type,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @throws \SoapFault
     * @throws \yii\db\Exception
     */
    public function actionSynchronize($type)
    {
        (new Dictionaries())->synchronize($type);
        return $this->redirect(['/dictionaries/index','type'=>$type]);
    }
}