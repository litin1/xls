<?php

namespace app\controllers;

use app\models\ConvertFile;
use app\models\enums\OrderStatus;
use app\models\Orders;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\Response;

class TransportController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['logout', 'index'],
                'rules' => [
                    [
                        'actions' => ['logout', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()

    {
        $files = scandir('transport/out');
        $orders = Orders::find()->where(['status'=>OrderStatus::IN_WORK])->all();
        foreach ($orders as $order) {
            if (!in_array($order->xls3,$files)) {
               $order->status = OrderStatus::LOADED;
               $order->save();
            }
        }
        return $this->render('index');
    }

    public function actionIndex2()
    {
        return $this->render('index2');

    }


    public function actionProcessing($file)
    {
        $orderNum = mb_substr($file,5,mb_strlen($file)-12);
        ConvertFile::readGTIN('transport/in/' . $file,'upload/' . $orderNum . '/'.'заказ' .$orderNum.'-1.xlsx');
        rename('transport/in/' . $file,'upload/' . $orderNum . '/'.'заказ' .$orderNum.'-2.xlsx');
        $order = Orders::find()->where(['id'=>$orderNum])->one();
        $order->status = OrderStatus::COMPLETE;
        $order->save();
        return $this->redirect(['transport/index2']);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

}

