<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%importers}}`.
 */
class m210717_173906_create_orders_item_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%orders_item}}', [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer()->comment('Заказ'),
            'count_pack' => $this->integer()->comment('Количество коробов'),
            'count_size' => $this->integer()->comment('Количество данного размера'),
            'gtin' => $this->string()->comment('GTIN'),
            'code_system' => $this->string(128)->comment('Код в учетной системе'),
            'manufacturer_code' => $this->string(50)->comment('Модель производителя'),
            'publication_date' => $this->integer()->comment('Дата публикации'),
            'prod_desc' => $this->string(1024)->comment('Наименование товара'),
            'prod_name' => $this->string(128)->comment('Бренд'),
            'prod_cover_type_dict' => $this->string(128)->comment('Тип упаковки'),
            'prod_cover_material' => $this->string(128)->comment('Материал упаковки'),
            'prod_count' => $this->decimal(19, 3)->comment('Количество/мера нетто'),
            'prod_measure' => $this->decimal(19, 3)->comment('Количество/мера нетто - ед.измерения'),
            'inn' => $this->string(20)->comment('ИНН импортера'),
            'country_id' => $this->integer()->comment('Страна производства'),
            'shoes_type' => $this->integer()->comment('Вид обуви'),
            'top_material_id' => $this->integer()->comment('Материал верха'),
            'substrate_material_id' => $this->integer()->comment('Материал подкладки'),
            'bottom_material_id' => $this->integer()->comment('Материал низа'),
            'color' => $this->string()->comment('Цвет'),
            'dimension_id' => $this->integer()->comment('Размер'),
            'tnved' => $this->integer()->comment('ТНВЕД'),
            'prod_gcpcl_brick' => $this->integer()->comment('Класификатор GCPCL BRICK')
        ]);
        $this->addForeignKey('fk-orders_item-order_id-orders-id', '{{%orders_item}}', 'order_id', '{{orders}}', 'id');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%orders_item}}');
    }
}
