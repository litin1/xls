св<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%importers}}`.
 */
class m210718_203906_create_settings_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%settings}}', [
            'id' => $this->primaryKey(),
            'key' => $this->string()->comment('Ключ'),
            'label' => $this->string()->comment('Наименование'),
            'value' => $this->string()->comment('Значенние'),
        ]);
        $this->insert('settings',['key'=>'xls_output1','label'=>'Имя исходного файла 1','value'=>'order1']);
        $this->insert('settings',['key'=>'xls_output2','label'=>'Имя исходного файла 2','value'=>'order2']);
        $this->insert('settings',['key'=>'user_output_file_access','label'=>'Доступ пользователя к файлам (0-нет/1-да)','value'=>'0']);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%settings}}');
    }
}
