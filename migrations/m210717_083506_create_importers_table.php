<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%importers}}`.
 */
class m210717_083506_create_importers_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%importers}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Название'),
            'inn' => $this->string(20)->comment('ИНН'),
            'gs1_login' => $this->string()->comment('Логин GS1'),
            'gs1_password' => $this->string()->comment('Пароль GS1')
        ]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%importers}}');
    }
}
