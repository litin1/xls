<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%importers}}`.
 */
class m210717_173506_create_orders_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%orders}}', [
            'id' => $this->primaryKey(),
            'importer_id' => $this->integer()->comment('Импортер'),
            'load_date' => $this->integer()->comment('Дата загрузки'),
            'status' => $this->integer()->comment(''),
            'draft_date' => $this->integer()->comment('Дата черновика'),
            'public_date' => $this->integer()->comment('Дата публикации'),
            'count_position' => $this->integer()->comment('Кол-во позиций до обработки'),
            'count_position2' => $this->integer()->comment('Кол-во позиций после обработки'),
            'user_id' => $this->integer()->comment('Пользователь'),
            'xls1' => $this->string()->comment('Входящий файл'),
            'xls2' => $this->string()->comment('Исходящий файл1'),
            'xls3' => $this->string()->comment('Исходящий файл2'),
        ]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%orders}}');
    }
}
