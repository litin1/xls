<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%importers}}`.
 */
class m210718_173906_create_gcpcl_brick_mapping_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%gcpcl_brick_mapping}}', [
            'id' => $this->primaryKey(),
            'mdo_type' => $this->string()->comment('Код'),
            'valdictpar' => $this->string(128)->comment('Название'),
            'gpc' => $this->string(50)->comment('GPC Код'),
            'gpc_valdictpar' => $this->string(1024)->comment('GPC Название'),
        ]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%gcpcl_brick_mapping}}');
    }
}
