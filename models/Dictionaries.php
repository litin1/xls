<?php

namespace app\models;

use app\models\gs1\GetDictionary;
use Yii;

/**
 * This is the model class for table "countries".
 *
 * @property string $id
 * @property int|null $intId
 * @property string|null $text
 * @property int|null $type
 */
class Dictionaries extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'dictionaries';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['intId'], 'integer'],
            [['id', 'text'], 'string', 'max' => 255],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Код',
            'intId' => 'ID',
            'text' => 'Название',
        ];
    }

    /**
     * @throws \SoapFault
     * @throws \yii\db\Exception
     */
    public function synchronize($type)
    {
        Yii::$app->db->createCommand()->delete('dictionaries', ['type' => $type])->execute(); // Удаляем все записи

        foreach (GetDictionary::getDictionary($type) as $country) {
            Yii::$app->db->createCommand()->insert('dictionaries', [
                'id' => $country->id,
                'intId' => $country->intId,
                'text' => $country->text,
                'type' => $type,
            ])->execute();
        }
    }
}
