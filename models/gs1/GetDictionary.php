<?php

namespace app\models\gs1;

use app\models\enums\DictionaryType;
use SoapClient;
use SoapFault;
use stdClass;

class GetDictionary
{
    private $attrId = [];

    /**
     * @return mixed
     * @throws SoapFault
     */
    public static function getDictionary($type)
    {
        $dictionary = new self;
        $dictionary->fillAttrIdList();
        $client = $dictionary->createSOAP();
        $d = new stdClass();
        $d->dictId = $dictionary->attrId[$type];
        $d->lang = 'RU';
        $d->noCache = false;
        $result = $client->GetDictionary($d);
        return $result->GS46Dictionary->DictList->Dict->Values->value;
    }

    private function fillAttrIdList()
    {
        $this->attrId[DictionaryType::COUNTRY] = 'WEB_90000187';
        $this->attrId[DictionaryType::SHOES_TYPE] = 'WEB_90001683';
        $this->attrId[DictionaryType::TOP_MATERIAL] = 'WEB_90001685';
        $this->attrId[DictionaryType::SUBSTRATE_MATERIAL] = 'WEB_90001686';
        $this->attrId[DictionaryType::BOTTOM_MATERIAL] = 'WEB_90001687';
        $this->attrId[DictionaryType::COLOR] = 'WEB_90001688';
        $this->attrId[DictionaryType::DIMENSION] = 'WEB_90001690';
    }

    /**
     * @throws SoapFault
     */
    private function createSOAP()
    {
        $login = "4660077009991";
        $password = "s43481395";
        ini_set('default_socket_timeout', 10000);
        $wsdlUrl = 'http://srs.gs1ru.org/GS46_Interfaces/GS1RU_Operations?wsdl';

        return new SoapClient($wsdlUrl, [
            'login' => $login,
            'password' => $password,
            'trace' => true,
        ]);
    }
}