<?php

namespace app\models;

/**
 * This is the model class for table "settings".
 *
 * @property int $id
 * @property string|null $key Ключ
 * @property string|null $label Наименование
 * @property string|null $value Значенние
 */
class Settings extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'settings';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['key', 'label', 'value'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'key' => 'Ключ',
            'label' => 'Наименование',
            'value' => 'Значенние',
        ];
    }

    public static function getValueFromKey($key)
    {
        /* @var $param \app\models\Settings */
        $param = self::find()->where(['key' => $key])->one();
        if ($param) {
            return $param->value;
        }
        return null;
    }
}
