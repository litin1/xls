<?php


namespace app\models;


use DateTime;

class Person
{
    private $id;
    private $name;
    private $surname;
    private $sex;
    private $birthDate;


    public function __construct($id, $name, $surname, $sex, $birthDate)
    {
        $this->id = $id;
        $this->name = $name;
        $this->surname = $surname;
        $this->sex = $sex;
        $this->birthDate = $birthDate;
    }

    /**
     * @throws \Exception
     */
    public function getAge()
    {
        $birthDay = new DateTime($this->birthDate);
        $currentDate = new DateTime();
        return $birthDay->diff($currentDate)->days;

    }

    public function __get($property)
    {
        return $this->$property;
    }


}