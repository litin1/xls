<?php

namespace app\models;

/**
 * This is the model class for table "gcpcl_brick_mapping".
 *
 * @property int $id
 * @property string|null $mdo_type Код
 * @property string|null $valdictpar Название
 * @property string|null $gpc GPC Код
 * @property string|null $gpc_valdictpar GPC Название
 */
class GcpclBrickMapping extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'gcpcl_brick_mapping';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['mdo_type'], 'string', 'max' => 255],
            [['valdictpar'], 'string', 'max' => 128],
            [['gpc'], 'string', 'max' => 50],
            [['gpc_valdictpar'], 'string', 'max' => 1024],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'mdo_type' => 'Код',
            'valdictpar' => 'Название',
            'gpc' => 'GPC Код',
            'gpc_valdictpar' => 'GPC Название',
        ];
    }
}
