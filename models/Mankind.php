<?php


namespace app\models;

use ArrayAccess;
use Iterator;

class Mankind implements ArrayAccess, Iterator
{
    protected $mankind = array();
    protected $_position = 1;

    private $countPerson = 0;
    private $countMale = 0;

    private static $instances = [];

    protected function __construct() { }

    protected function __clone() { }

    public function __wakeup()
    {
        throw new \Exception("Cannot unserialize a singleton.");
    }

    public static function getInstance()
    {
        $class = static::class;
        if (!isset(self::$instances[$class])) {
            self::$instances[$class] = new static();
        }

        return self::$instances[$class];
    }

    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->mankind[] = $value;
        } else {
            $this->mankind[$offset] = $value;
        }
    }

    public function offsetExists($offset)
    {
        return isset($this->mankind[$offset]);
    }

    public function offsetUnset($offset)
    {
        if (isset($this->mankind[$offset])) {
            unset($this->mankind[$offset]);
        }

    }

    public function offsetGet($offset)
    {
        return isset($this->mankind[$offset]) ? $this->mankind[$offset] : null;
    }

    public function current()
    {
        return $this->mankind[$this->_position];

    }

    public function key()
    {
        return $this->_position;
    }

    public function next()
    {
        if (next($this->mankind) !== false) {
            $this->_position = key($this->mankind);
        } else {
            $this->_position = null;
        }
    }

    public function rewind()
    {
        if ($this->mankind) {
            reset($this->mankind);
            $this->_position = key($this->mankind);
        }


    }

    public function valid()
    {
        if (isset($this->mankind[$this->_position])) {
            return null !== $this->mankind[$this->_position];
        }
        return false;
    }

    public function load($filename, $offset = 0, $limit = 0)
    {
        $f = fopen($filename, 'r');

        if ($f) {
            $indexStr = 0;
            if ($offset) {
                while ((fgets($f, 4096) !== false) && ($indexStr < $offset - 1)) {
                    $indexStr++;
                }
                $indexStr = 0;
            }
            while ((($str = fgets($f, 4096)) !== false) && ($indexStr <= $limit - 1)) {
                $personData = explode(';', $str);
                $this->mankind[$personData[0]] = new Person(
                    $personData[0],
                    $personData[1],
                    $personData[2],
                    $personData[3],
                    $personData[4]
                );
                if ($personData[3] == 'M') {
                    $this->countMale++;
                }
                $indexStr++;
            }
            $this->countPerson = $indexStr;
            fclose($f);
        }
    }

    public function getMalePercent() {
        return $this->countMale/$this->countPerson*100;
    }
}
