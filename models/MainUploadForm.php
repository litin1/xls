<?php

namespace app\models;

use app\models\enums\OrderStatus;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 *
 * @property-read User|null $user This property is read-only.
 *
 */
class MainUploadForm extends Model
{
    public $importer_id;
    public $file;
    public $xls;
    public $xls1;
    public $xls2;
    public $inxls1;
    public $inxls2;



    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['xls', 'xls1', 'xls2'], 'safe'],
            [['importer_id', 'xls'], 'required']
        ];
    }

    public function attributeLabels()
    {
        return [
            'importer_id' => 'Импортер',
            'xls' => 'Входной файл',
            'inxls1' => 'Входной файл',
            'inxls2' => 'Входной файл',
            'xls1' => 'Исходящий файл1',
            'xls2' => 'Исходящий файл2'
        ];
    }

    public function save($user_id)
    {
        $this->file->saveAs("upload/input.xls");

        $d = new ConvertFile();
        $d->inn = Importers::findOne($this->importer_id)->inn;
        $d->ReadInputXls();
        $d->createOutput1();
        $d->createOutput2();

        $order = new Orders();
        $order->user_id = $user_id;
        $order->load_date = time();
        $order->importer_id = $this->importer_id;
        $order->status = OrderStatus::LOADED;
        $order->count_position = count($d->inputData);
        $order->save();

        $dir = $order->id;
        if (!file_exists("upload/" . $dir)) {
            mkdir("upload/" . $dir,0777);
        }
        $order->xls1 = $this->file->getBaseName() . '.xls';
        $order->xls2 = 'заказ' .$order->id.'-1.xlsx';
        $order->xls3 = 'заказ' .$order->id.'-2.xlsx';
        $order->save();

        $this->xls1 = $order->xls2;
        $this->xls2 = $order->xls3;
        rename('upload/input.xls', 'upload/' . $dir . '/' . $this->file->getBaseName() . '.xls');
        //copy('upload/output2.xlsx', 'transport/out/'.$order->xls3);
        //chmod('transport/out/'.$order->xls3,0777);
        rename('upload/output1.xlsx', 'upload/' . $dir . '/'.$order->xls2);
        rename('upload/output2.xlsx', 'upload/' . $dir . '/'.$order->xls3);

    }


}
