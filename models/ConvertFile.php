<?php


namespace app\models;


use PhpOffice\PhpSpreadsheet\Cell\DataType;
use PhpOffice\PhpSpreadsheet\IOFactory;

class ConvertFile
{
    public $inputFile = 'upload/input.xls';
    public $outputTemplateFile1 = 'template/output1.xlsx';
    public $outputFile1 = 'upload/output1.xlsx';
    public $outputTemplateFile2 = 'template/output2.xlsx';
    public $outputFile2 = 'upload/output2.xlsx';
    public $inputData;
    public $cellsList;
    public $inn;

    public $currentRow = 7;

    /**
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     */
    public function ReadInputXls()
    {
        $this->cellsList = [
            1 => 'A',
            2 => 'B',
            3 => 'C',
            4 => 'D',
            5 => 'E',
            6 => 'F',
            7 => 'G',
            8 => 'H',
            9 => 'I',
            10 => 'I',
            11 => 'J',
            12 => 'K',
            13 => 'L',
            14 => 'M',
            15 => 'N',
            16 => 'O',
            17 => 'P',
            18 => 'Q',
            19 => 'R'
        ];

        $reader = IOFactory::createReaderForFile($this->inputFile);
        $spreadsheet = $reader->load($this->inputFile);
        $cells = $spreadsheet->getActiveSheet()->getCellCollection();
        $row = 3;
        $fl = true;
        while ($fl) {
            if ($cells->get('B' . $row) == '') {
                $fl = false;
                break;
            }
            for ($col = 1; $col < 20; $col++) {
                $array[$row][$this->cellsList[$col]] = ($cells->get($this->cellsList[$col] . $row)) ?
                    $cells->get($this->cellsList[$col] . $row)->getValue() : '';
            }
            $row++;
        }
        $this->inputData = $array;
        unset($reader);
    }

    /**
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     */
    public function createOutput1()
    {
        copy($this->outputTemplateFile1, $this->outputFile1);
        $reader = IOFactory::createReaderForFile($this->outputFile1);
        $spreadsheet = $reader->load($this->outputFile1);
        $worksheet = $spreadsheet->setActiveSheetIndex(0);
        for ($row = 3; $row < count($this->inputData) + 3; $row++) {
            $sizeList = $this->parseSize($this->inputData[$row]['D']);
            $countList = $this->parseCount($this->inputData[$row]['D']);
            $this->insertArticle1($worksheet, $row, $sizeList, $countList);
        }
        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save($this->outputFile1);
        unset($reader);

    }

    private function insertArticle1($workSheet, $id, $sizeList, $countList)
    {
        $i = 0;
        foreach ($sizeList as $size) {
            $excel_cell = $workSheet->getCell('D' . $this->currentRow)
                ->setValueExplicit($this->inputData[$id]['B'], DataType::TYPE_STRING);
            $excel_cell = $workSheet->getCell('E' . $this->currentRow)
                ->setValueExplicit(date('d.m.Y', strtotime(date('Y-m-d') . " - 1 days")), DataType::TYPE_STRING);
            $f = $this->inputData[$id]['I'] . ', ' . $this->inputData[$id]['J'] . ', ' . $this->cutMaterial($this->inputData[$id]['K']) .
                ', ' . $this->inputData[$id]['H'] . ', МОДЕЛЬ ' . $this->inputData[$id]['B'] . ', р.' . $size;
            $excel_cell = $workSheet->getCell('F' . $this->currentRow)
                ->setValueExplicit($f, DataType::TYPE_STRING);
            $excel_cell = $workSheet->getCell('G' . $this->currentRow)
                ->setValueExplicit($this->inputData[$id]['C'], DataType::TYPE_STRING);
            $excel_cell = $workSheet->getCell('H' . $this->currentRow)
                ->setValueExplicit($this->inn, DataType::TYPE_STRING);
            $excel_cell = $workSheet->getCell('I' . $this->currentRow)
                ->setValueExplicit('<CN> КИТАЙ', DataType::TYPE_STRING);
            $excel_cell = $workSheet->getCell('J' . $this->currentRow)
                ->setValueExplicit(DictionaryService::getTypeShoe($this->inputData[$id]['J']), DataType::TYPE_STRING);
            $excel_cell = $workSheet->getCell('K' . $this->currentRow)
                ->setValueExplicit($this->inputData[$id]['K'], DataType::TYPE_STRING);
            $excel_cell = $workSheet->getCell('L' . $this->currentRow)
                ->setValueExplicit($this->inputData[$id]['M'], DataType::TYPE_STRING);
            $excel_cell = $workSheet->getCell('M' . $this->currentRow)
                ->setValueExplicit($this->inputData[$id]['N'], DataType::TYPE_STRING);
            $excel_cell = $workSheet->getCell('N' . $this->currentRow)
                ->setValueExplicit($this->inputData[$id]['H'], DataType::TYPE_STRING);
            $excel_cell = $workSheet->getCell('O' . $this->currentRow)
                ->setValueExplicit(DictionaryService::getTypeSize($size), DataType::TYPE_STRING);
            $excel_cell = $workSheet->getCell('P' . $this->currentRow)
                ->setValueExplicit($this->getTNVED($this->inputData[$id]['K']), DataType::TYPE_STRING);
            $this->currentRow++;
            $i++;
        }

    }

    public function createOutput2()
    {
        $this->currentRow = 7;
        copy($this->outputTemplateFile2, $this->outputFile2);
        $reader = IOFactory::createReaderForFile($this->outputFile2);
        $spreadsheet = $reader->load($this->outputFile2);
        $worksheet = $spreadsheet->setActiveSheetIndex(0);
        for ($row = 3; $row < count($this->inputData) + 3; $row++) {
            $sizeList = $this->parseSize($this->inputData[$row]['D']);
            $countList = $this->parseCount($this->inputData[$row]['D']);
            $this->insertArticle2($worksheet, $row, $sizeList, $countList);
        }
        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save($this->outputFile2);
        unset($reader);
    }

    private function insertArticle2($workSheet, $id, $sizeList, $countList)
    {
        $i = 0;
        foreach ($sizeList as $size) {
            $excel_cell = $workSheet->getCell('A' . $this->currentRow)
                ->setValueExplicit((int)$this->inputData[$id]['G'], DataType::TYPE_NUMERIC);
            $excel_cell = $workSheet->getCell('B' . $this->currentRow)
                ->setValueExplicit((int)$this->inputData[$id]['G'] * (int)$countList[$i], DataType::TYPE_NUMERIC);
            $excel_cell = $workSheet->getCell('D' . $this->currentRow)
                ->setValueExplicit($countList[$i], DataType::TYPE_NUMERIC);
            $excel_cell = $workSheet->getCell('E' . $this->currentRow)
                ->setValueExplicit($this->inputData[$id]['B'], DataType::TYPE_STRING);
            $excel_cell = $workSheet->getCell('F' . $this->currentRow)
                ->setValueExplicit(date('d.m.Y', strtotime(date('Y-m-d') . " - 1 days")), DataType::TYPE_STRING);
            $excel_cell = $workSheet->getCell('H' . $this->currentRow)
                ->setValueExplicit($this->inputData[$id]['C'], DataType::TYPE_STRING);
            $excel_cell = $workSheet->getCell('I' . $this->currentRow)
                ->setValueExplicit($this->inn, DataType::TYPE_STRING);
            $excel_cell = $workSheet->getCell('J' . $this->currentRow)
                ->setValueExplicit('<CN> КИТАЙ', DataType::TYPE_STRING);
            $excel_cell = $workSheet->getCell('K' . $this->currentRow)
                ->setValueExplicit(DictionaryService::getTypeShoe($this->inputData[$id]['J']), DataType::TYPE_STRING);
            $excel_cell = $workSheet->getCell('L' . $this->currentRow)
                ->setValueExplicit($this->inputData[$id]['K'], DataType::TYPE_STRING);
            $excel_cell = $workSheet->getCell('M' . $this->currentRow)
                ->setValueExplicit($this->inputData[$id]['M'], DataType::TYPE_STRING);
            $excel_cell = $workSheet->getCell('N' . $this->currentRow)
                ->setValueExplicit($this->inputData[$id]['N'], DataType::TYPE_STRING);
            $excel_cell = $workSheet->getCell('O' . $this->currentRow)
                ->setValueExplicit($this->inputData[$id]['H'], DataType::TYPE_STRING);
            $excel_cell = $workSheet->getCell('P' . $this->currentRow)
                ->setValueExplicit(DictionaryService::getTypeSize($size), DataType::TYPE_STRING);
            $excel_cell = $workSheet->getCell('Q' . $this->currentRow)
                ->setValueExplicit($this->getTNVED($this->inputData[$id]['K']), DataType::TYPE_STRING);
            $f = $this->inputData[$id]['I'] . ', ' . $this->inputData[$id]['J'] . ', ' . $this->cutMaterial($this->inputData[$id]['K']) .
                ', ' . $this->inputData[$id]['H'] . ', МОДЕЛЬ ' . $this->inputData[$id]['B'] . ', р.' . $size;
            $excel_cell = $workSheet->getCell('G' . $this->currentRow)
                ->setValueExplicit($f, DataType::TYPE_STRING);
            $this->currentRow++;
            $i++;
        }
    }

    private function parseSize($sizeString)
    {
        $sizeString = substr($sizeString, 0, strpos($sizeString, '('));
        return explode('-', $sizeString);
    }

    private function parseCount($countString)
    {
        $countString = substr($countString, strpos($countString, '(') + 1);
        $countString = substr($countString, 0, -1);
        return explode('-', $countString);
    }

    private function cutMaterial($material)
    {
        if (mb_strtoupper($material) == 'ИСКУССТВЕННАЯ КОЖА') {
            return 'КЗ';
        } elseif (mb_strtoupper($material) == 'ТЕКСТИЛЬ') {
            return 'текст';
        } else {
            return 'кожа';
        }
    }

    private function getTNVED($material)
    {
        if (mb_strtoupper($material) == 'ИСКУССТВЕННАЯ КОЖА') {
            return '<6402919000> --- прочая';
        } elseif (mb_strtoupper($material) == 'ТЕКСТИЛЬ') {
            return '<6404199000> --- прочая';
        } else {
            return '';
        }
    }

    public static function readGTIN($input,$output)
    {
        $reader = IOFactory::createReaderForFile($input);
        $spreadsheet = $reader->load($input);
        $cells = $spreadsheet->getActiveSheet()->getCellCollection();
        $row = 7;
        $fl = true;
        $array = [];
        while ($fl) {
            if ($cells->get('C' . $row) == '') {
                $fl = false;
                break;
            }
            $array[$row] = ($cells->get('C' . $row));
            $row++;

        }
        unset($reader);

        $reader = IOFactory::createReaderForFile($output);
        $spreadsheet = $reader->load($output);
        $workSheet = $spreadsheet->setActiveSheetIndex(0);
        for ($row = 7; $row < count($array) + 7; $row++) {
            $excel_cell = $workSheet->getCell('B' . $row)
                ->setValueExplicit($array[$row], DataType::TYPE_STRING);
        }
        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save($output);
        unset($reader);
    }
}