<?php

namespace app\models;

/**
 * This is the model class for table "orders".
 *
 * @property int $id
 * @property int|null $importer_id Импортер
 * @property int|null $load_date Дата загрузки
 * @property int|null $status
 * @property int|null $draft_date Дата черновика
 * @property int|null $public_date Дата публикации
 * @property int|null $count_position Количество позиций
 * @property int|null $user_id Пользователь
 *
 * @property OrdersItem[] $ordersItems
 */
class Orders extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'orders';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['importer_id', 'load_date', 'status', 'draft_date', 'public_date', 'count_position','user_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => '№ заказа',
            'importer_id' => 'Импортер',
            'load_date' => 'Дата загрузки',
            'status' => 'Статус',
            'draft_date' => 'Дата черновика',
            'public_date' => 'Дата публикации',
            'count_position' => 'Кол-во позиций до обработки',
            'count_position_after' => 'Кол-во позиций после обработки',
            'user_id' => 'Пользователь'
        ];
    }

    /**
     * Gets query for [[OrdersItems]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getOrdersItems()
    {
        return $this->hasMany(OrdersItem::className(), ['order_id' => 'id']);
    }
}
