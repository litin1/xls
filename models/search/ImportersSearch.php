<?php

namespace app\models\search;

use app\models\Importers;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * ImportersSearch represents the model behind the search form about `app\models\Importers`.
 */
class ImportersSearch extends Importers
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['name', 'inn', 'gs1_login', 'gs1_password'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Importers::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'inn', $this->inn])
            ->andFilterWhere(['like', 'gs1_login', $this->gs1_login])
            ->andFilterWhere(['like', 'gs1_password', $this->gs1_password]);

        return $dataProvider;
    }
}
