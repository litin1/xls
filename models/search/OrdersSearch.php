<?php

namespace app\models\search;

use app\models\Orders;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * OrdersSearch represents the model behind the search form of `app\models\Orders`.
 */
class OrdersSearch extends Orders
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'importer_id', 'load_date', 'status', 'draft_date', 'public_date', 'count_position','user_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $user_id = null)
    {
        $query = Orders::find();
        if ($user_id) {
            $query->andWhere(['user_id' => $user_id]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);


        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'importer_id' => $this->importer_id,
            'user_id' => $this->user_id,
            'load_date' => $this->load_date,
            'status' => $this->status,
            'draft_date' => $this->draft_date,
            'public_date' => $this->public_date,
            'count_position' => $this->count_position,
        ]);

        return $dataProvider;
    }
}
