<?php

namespace app\models\search;

use app\models\Dictionaries;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * CountriesSearch represents the model behind the search form about `app\models\Countries`.
 */
class DictionariesSearch extends Dictionaries
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'text'], 'safe'],
            [['intId', 'type'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($type, $params)
    {
        $query = Dictionaries::find()->where(['type' => $type]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'intId' => $this->intId,
        ]);

        $query->andFilterWhere(['like', 'id', $this->id])
            ->andFilterWhere(['like', 'text', $this->text]);

        return $dataProvider;
    }
}
