<?php

namespace app\models\search;

use app\models\GcpclBrickMapping;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * GcpclBrickMappingSearch represents the model behind the search form about `app\models\GcpclBrickMapping`.
 */
class GcpclBrickMappingSearch extends GcpclBrickMapping
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['mdo_type', 'valdictpar', 'gpc', 'gpc_valdictpar'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = GcpclBrickMapping::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'mdo_type', $this->mdo_type])
            ->andFilterWhere(['like', 'valdictpar', $this->valdictpar])
            ->andFilterWhere(['like', 'gpc', $this->gpc])
            ->andFilterWhere(['like', 'gpc_valdictpar', $this->gpc_valdictpar]);

        return $dataProvider;
    }
}
