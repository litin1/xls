<?php

namespace app\models\enums;

use yii2mod\enum\helpers\BaseEnum;

/**
 * Роли пользователей
 */
class UserRole extends BaseEnum
{
    const ADMIN = 2;
    const USER = 1;

    public static $list = [
        self::ADMIN => 'Администратор',
        self::USER => 'Пользователь',
    ];
}