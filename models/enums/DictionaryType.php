<?php

namespace app\models\enums;

use yii2mod\enum\helpers\BaseEnum;

/**
 * Тип справочников
 */
class DictionaryType extends BaseEnum
{
    const COUNTRY = 1;
    const SHOES_TYPE = 2;
    const TOP_MATERIAL = 3;
    const SUBSTRATE_MATERIAL = 4;
    const BOTTOM_MATERIAL = 5;
    const COLOR = 6;
    const DIMENSION = 7;

    public static $list = [
        self::COUNTRY => 'Страни',
        self::SHOES_TYPE => 'Тип обуви',
        self::TOP_MATERIAL => 'Материал верха',
        self::SUBSTRATE_MATERIAL => 'Материал подкладки',
        self::BOTTOM_MATERIAL => 'Материал низа',
        self::COLOR => 'Цвет',
        self::DIMENSION => 'Размер',
    ];
}