<?php

namespace app\models\enums;

use yii2mod\enum\helpers\BaseEnum;

/**
 * Роли пользователей
 */
class OrderStatus extends BaseEnum
{
    const LOADED = 1;
    const IN_WORK = 2;
    const COMPLETE = 3;

    public static $list = [
        self::LOADED => 'Загружен',
        self::IN_WORK => 'В работе',
        self::COMPLETE => 'Завершен',
    ];
}