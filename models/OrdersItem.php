<?php

namespace app\models;

/**
 * This is the model class for table "orders_item".
 *
 * @property int $id
 * @property int|null $order_id Заказ
 * @property int|null $count_pack Количество коробов
 * @property int|null $count_size Количество данного размера
 * @property string|null $gtin GTIN
 * @property string|null $code_system Код в учетной системе
 * @property string|null $manufacturer_code Модель производителя
 * @property int|null $publication_date Дата публикации
 * @property string|null $prod_desc Наименование товара
 * @property string|null $prod_name Бренд
 * @property string|null $prod_cover_type_dict Тип упаковки
 * @property string|null $prod_cover_material Материал упаковки
 * @property float|null $prod_count Количество/мера нетто
 * @property float|null $prod_measure Количество/мера нетто - ед.измерения
 * @property string|null $inn ИНН импортера
 * @property int|null $country_id Страна производства
 * @property int|null $shoes_type Вид обуви
 * @property int|null $top_material_id Материал верха
 * @property int|null $substrate_material_id Материал подкладки
 * @property int|null $bottom_material_id Материал низа
 * @property string|null $color Цвет
 * @property int|null $dimension_id Размер
 * @property int|null $tnved ТНВЕД
 * @property int|null $prod_gcpcl_brick Класификатор GCPCL BRICK
 *
 * @property Orders $order
 */
class OrdersItem extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'orders_item';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['order_id', 'count_pack', 'count_size', 'publication_date', 'country_id', 'shoes_type', 'top_material_id', 'substrate_material_id', 'bottom_material_id', 'dimension_id', 'tnved', 'prod_gcpcl_brick'], 'integer'],
            [['prod_count', 'prod_measure'], 'number'],
            [['gtin', 'color'], 'string', 'max' => 255],
            [['code_system', 'prod_name', 'prod_cover_type_dict', 'prod_cover_material'], 'string', 'max' => 128],
            [['manufacturer_code'], 'string', 'max' => 50],
            [['prod_desc'], 'string', 'max' => 1024],
            [['inn'], 'string', 'max' => 20],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Orders::className(), 'targetAttribute' => ['order_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'Заказ',
            'count_pack' => 'Количество коробов',
            'count_size' => 'Количество данного размера',
            'gtin' => 'GTIN',
            'code_system' => 'Код в учетной системе',
            'manufacturer_code' => 'Модель производителя',
            'publication_date' => 'Дата публикации',
            'prod_desc' => 'Наименование товара',
            'prod_name' => 'Бренд',
            'prod_cover_type_dict' => 'Тип упаковки',
            'prod_cover_material' => 'Материал упаковки',
            'prod_count' => 'Количество/мера нетто',
            'prod_measure' => 'Количество/мера нетто - ед.измерения',
            'inn' => 'ИНН импортера',
            'country_id' => 'Страна производства',
            'shoes_type' => 'Вид обуви',
            'top_material_id' => 'Материал верха',
            'substrate_material_id' => 'Материал подкладки',
            'bottom_material_id' => 'Материал низа',
            'color' => 'Цвет',
            'dimension_id' => 'Размер',
            'tnved' => 'ТНВЕД',
            'prod_gcpcl_brick' => 'Класификатор GCPCL BRICK',
        ];
    }

    /**
     * Gets query for [[Order]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Orders::className(), ['id' => 'order_id']);
    }
}
