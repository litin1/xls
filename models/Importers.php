<?php

namespace app\models;

use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "importers".
 *
 * @property int $id
 * @property string|null $name Название
 * @property string|null $inn ИНН
 * @property string|null $gs1_login Логин GS1
 * @property string|null $gs1_password Пароль GS1
 */
class Importers extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'importers';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'gs1_login', 'gs1_password'], 'string', 'max' => 255],
            [['inn'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'inn' => 'ИНН',
            'gs1_login' => 'Логин GS1',
            'gs1_password' => 'Пароль GS1',
        ];
    }

    public static function getImportersList () {
        return ArrayHelper::map(self::find()->all(),'id','name');
    }
}
