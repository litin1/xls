<?php

use johnitvn\ajaxcrud\CrudAsset;
use kartik\grid\GridView;
use yii\bootstrap\Modal;
use yii\helpers\Html;

/* @var $this yii\web\View
 * @var $searchModel app\models\DictionariesSearch
 * @var $dataProvider yii\data\ActiveDataProvider
 * @var $type integer
 */


$this->title = \app\models\enums\DictionaryType::getLabel($type);
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

?>
<div class="countries-index">
    <div id="ajaxCrudDatatable">
        <?= GridView::widget([
            'id' => 'crud-datatable',
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'pjax' => true,
            'columns' => [
                'id',
                'intId',
                'text'
            ],
            'toolbar' => [
                [
                    'content' =>
                        Html::a('<i class="glyphicon glyphicon-refresh"></i> Синхронизация',
                            ['synchronize', 'type' => $type],
                            [
                                'title' => 'Синхронизировать по Апи',
                                'class' => 'btn btn-primary'
                            ]) .
                        Html::a('<i class="glyphicon glyphicon-repeat"></i>', [''],
                            ['data-pjax' => 1, 'class' => 'btn btn-default', 'title' => 'Reset Grid']) .
                        '{toggleData}' .
                        '{export}'
                ],
            ],
            'striped' => true,
            'condensed' => true,
            'responsive' => true,
            'panel' => [
                'type' => 'primary',
                'heading' => '<i class="glyphicon glyphicon-list"></i> '.$this->title,
                'before' => '',
                'after' => false,
            ]
        ]) ?>
    </div>
</div>
<?php Modal::begin([
    "id" => "ajaxCrudModal",
    "footer" => "",// always need it for jquery plugin
]) ?>
<?php Modal::end(); ?>
