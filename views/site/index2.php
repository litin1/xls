<?php

/* @var $this yii\web\View
 * @var $model MainUploadForm
 * @var $dataProvider ActiveDataProvider
 * @var $searchModel OrdersSearch
 */

use app\models\enums\OrderStatus;
use app\models\MainUploadForm;
use app\models\Orders;
use app\models\search\OrdersSearch;
use johnitvn\ajaxcrud\CrudAsset;
use kartik\grid\GridView;
use yii\bootstrap\Modal;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\helpers\Url;

CrudAsset::register($this);
$showFileAccess = \app\models\Settings::getValueFromKey('user_output_file_access');
$importers = \app\models\Importers::getImportersList();
$this->title = 'Конвертация';
?>

<div role="tabpanel">
    <br><br><br>
    <!-- Навігаційні елементи вкладок -->
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" ><?=Html::a('Загрузка',["site/index"])?></li>
        <li role="presentation" class="active"><?=Html::a('Заказы',["site/index2"])?></li>
    </ul>

    <!-- Вкладки панелі -->
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane" id="home">
        </div>
        <div role="tabpanel" class="tab-pane active" id="profile">
            <div class="countries-index">
                <div id="ajaxCrudDatatable">
                    <?= GridView::widget([
                        'id' => 'crud-datatable',
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'rowOptions' => function ($data) {
                            /* @var $data Orders */
                            if ($data->status == OrderStatus::COMPLETE) {
                                return ['style' => 'background-color:#fffebb'];
                            } elseif ($data->status == OrderStatus::IN_WORK) {
                                return ['style' => 'background-color:#bae1ff'];
                            } else {
                                return ['style' => 'background-color:#baffc9'];
                            }
                        },
                        'columns' => [
                            'id',
                            [
                                'attribute' => 'importer_id',
                                'filter' => $importers,
                                'value' => function ($data) use ($importers) {
                                    /* @var $data Orders */
                                    return $importers[$data->importer_id] ?? 'не задан';
                                }
                            ],
                            [
                                'attribute' => 'load_date',
                                'format' => ['date', 'php:d.m.Y'],
                            ],
                            [
                                'attribute' => 'status',
                                'filter' => OrderStatus::listData(),
                                'value' => function ($data) {
                                    /* @var $data Orders */
                                    return OrderStatus::getLabel($data->status);

                                }
                            ],
                            'count_position',
                            [
                                'class' => 'kartik\grid\ActionColumn',
                                'visible' => $showFileAccess == 1,
                                'header' => '&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspДействия&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp',
                                'dropdown' => false,
                                'vAlign' => 'middle',
                                'urlCreator' => function ($action, $model, $key, $index) {
                                    return Url::to([$action, 'id' => $key]);
                                },
                                'template' => '{view} &nbsp {upload-input} &nbsp {upload-output1} &nbsp {upload-output2}',
                                'buttons' => [
                                    'upload-input' => function ($url, $data, $key) {
                                        return Html::a('<i class="glyphicon glyphicon-floppy-save"></i>', '/upload/'.$key.'/' . $data->xls1);
                                    },
                                    'upload-output1' => function ($url, $data, $key) {
                                        return Html::a('<i class="glyphicon glyphicon-floppy-save"></i>', '/upload/'.$key.'/' . $data->xls2);
                                    },
                                    'upload-output2' => function ($url, $data, $key) {
                                        return Html::a('<i class="glyphicon glyphicon-floppy-save"></i>', '/upload/'.$key.'/' . $data->xls3);
                                    },
                                ],
                                'viewOptions' => ['role' => 'modal-remote', 'title' => 'View', 'data-toggle' => 'tooltip'],
                            ],
                        ],
                        'toolbar' => [
                            [
                                'content' =>

                                    Html::a('<i class="glyphicon glyphicon-repeat"></i>', [''],
                                        ['data-pjax' => 1, 'class' => 'btn btn-default', 'title' => 'Reset Grid']) .
                                    '{toggleData}' .
                                    '{export}'
                            ],
                        ],
                        'striped' => true,
                        'condensed' => true,
                        'responsive' => true,
                        'panel' => [
                            'type' => 'primary',
                            'heading' => '<i class="glyphicon glyphicon-list"></i> Заказы',
                            'before' => '',
                            'after' => false,
                        ]
                    ]) ?>
                </div>
            </div>
        </div>
    </div>

</div>



<?php Modal::begin([
    "id" => "ajaxCrudModal",
    "footer" => "",// always need it for jquery plugin
]) ?>
<?php Modal::end(); ?>
<?php
$script = <<<JS
$("#convert-xls").click(function() {
    console.log('asdas');
    $('.loading').show();
 });
JS;
$this->registerJs($script);
?>


