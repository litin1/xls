<?php

/* @var $this yii\web\View
 * @var $model MainUploadForm
 * @var $dataProvider ActiveDataProvider
 * @var $searchModel OrdersSearch
 */

use app\models\MainUploadForm;
use app\models\search\OrdersSearch;
use johnitvn\ajaxcrud\CrudAsset;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Modal;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\helpers\Url;

CrudAsset::register($this);
$showFileAccess = \app\models\Settings::getValueFromKey('user_output_file_access');
$importers = \app\models\Importers::getImportersList();
$this->title = 'Конвертация';
?>

<div role="tabpanel">
    <br><br><br>
    <!-- Навігаційні елементи вкладок -->
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active"><?= Html::a('Загрузка', ["/site/index"]) ?></li>
        <li role="presentation"><?= Html::a('Заказы', ["/site/index2"]) ?></li>
    </ul>

    <!-- Вкладки панелі -->
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="home">
            <div class="user-index">
                <div class="loading"
                     style="position: absolute; z-index:10000; top: 50%; left: 50%; transform: translate(-50%, -50%);display: none">
                    <?= Html::img('/css/loading.gif'); ?>
                </div>
                <?php $form = ActiveForm::begin() ?>
                <div class="row">

                    <div class="col-md-6"><h3 class="box-title">СКИФ</h3>

                        <div class="row">
                            <div class="col-md-6">
                                <?= $form->field($model, 'inxls1')->fileInput() ?>
                            </div>
                            <div class="col-md-6" style="padding-top: 25px">
                                <?= Html::submitButton('Конвертировать',
                                    [
                                        'class' => 'btn btn-primary convert-xls',
                                        'formaction' => Url::to([
                                            'site/' . Yii::$app->controller->action->id,
                                            'id' => 1
                                        ])
                                    ]); ?>
                            </div>

                        </div>
                    </div>
                    <div class="col-md-6">
                        <h3 class="box-title">АЛЬЯНС</h3>
                        <div class="row">
                            <div class="col-md-6">
                                <?= $form->field($model, 'inxls2')->fileInput() ?>
                            </div>
                            <div class="col-md-5" style="padding-top: 25px">
                                <?= Html::submitButton('Конвертировать',
                                    [
                                        'class' => 'btn btn-primary convert-xls',
                                        'formaction' => Url::to([
                                            'site/' . Yii::$app->controller->action->id,
                                            'id' => 2
                                        ])
                                    ]) ?>
                            </div>

                        </div>
                    </div>
                </div>
                <?php ActiveForm::end() ?>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="profile">
        </div>
    </div>

</div>


<?php Modal::begin([
    "id" => "ajaxCrudModal",
    "footer" => "",// always need it for jquery plugin
]) ?>
<?php Modal::end(); ?>
<?php
$script = <<<JS
$(".convert-xls").click(function() {
    console.log('asdas');
    $('.loading').show();
 });
JS;
$this->registerJs($script);
?>


