<?php

/* @var $this yii\web\View
 * @var $model MainUploadForm
 * @var $dataProvider ActiveDataProvider
 * @var $searchModel OrdersSearch
 */

use app\models\MainUploadForm;
use app\models\search\OrdersSearch;
use johnitvn\ajaxcrud\CrudAsset;
use yii\bootstrap\Modal;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;

CrudAsset::register($this);
$showFileAccess = \app\models\Settings::getValueFromKey('user_output_file_access');
$importers = \app\models\Importers::getImportersList();
$this->title = 'Обмен файлами';
$files = [];
$dir = 'transport/in/';
if ($handle = opendir($dir)) {
    while (false !== ($file = readdir($handle))) {
        if ($file != "." && $file != "..") {
            $files[] = [
                'name' => $file,
                'date' => date("d F Y H:i:s", filectime($dir . $file)),
                'size' => filesize($dir . $file) . ' байт',
            ];
        }
    }
} else {
    echo 'Ошибка открытия директории';
}
?>

<div role="tabpanel">
    <br><br><br>
    <!-- Навігаційні елементи вкладок -->
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" ><?= Html::a('Отправленные', ["/transport/index"]) ?></li>
        <li role="presentation" class="active"><?= Html::a('Полученные', ["/transport/index2"]) ?></li>
    </ul>

    <!-- Вкладки панелі -->
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="home">
            <div class="user-index">
                <div class="panel panel-primary">
                    <div class="panel panel-heading">
                        <div class="row">
                            <div class="col-xs-3">Название</div>
                            <div class="col-xs-2">Дата</div>
                            <div class="col-xs-1">Размер</div>
                        </div>
                    </div>
                    <div class="panel panel-body">
                        <?php foreach ($files as $file) : ?>
                            <div class="row">
                                <div class="col-xs-3"><?= $file['name'] ?></div>
                                <div class="col-xs-2"><?= $file['date'] ?></div>
                                <div class="col-xs-1"><?= $file['size'] ?></div>
                                <div class="col-xs-1"><?= Html::a('<i class="glyphicon glyphicon-qrcode"></i>',['transport/processing','file'=>$file['name']]) ?></div>
                            </div>

                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="profile">
        </div>
    </div>
</div>


<?php Modal::begin([
    "id" => "ajaxCrudModal",
    "footer" => "",// always need it for jquery plugin
]) ?>
<?php Modal::end(); ?>
<?php
$script = <<<JS
$(".convert-xls").click(function() {
    console.log('asdas');
    $('.loading').show();
 });
JS;
$this->registerJs($script);
?>


