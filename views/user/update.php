<?php

/* @var $this yii\web\View */

/* @var $model User */

use common\models\User;

$this->title = 'Редактировать пользователя';
?>
<div class="user-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
