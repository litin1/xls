<?php

use app\models\enums\OrderStatus;
use app\models\Importers;
use app\models\Orders;
use johnitvn\ajaxcrud\CrudAsset;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\OrdersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

CrudAsset::register($this);
$importers = Importers::getImportersList();
$users = ArrayHelper::map(\app\models\User::find()->all(), 'id', 'username');

$this->title = 'Orders';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="orders-index">

    <div id="ajaxCrudDatatable">
        <?= GridView::widget([
            'id' => 'crud-datatable',
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'rowOptions' => function ($data) {
                /* @var $data Orders */
                if ($data->status == OrderStatus::COMPLETE) {
                    return ['style' => 'background-color:#fffebb'];
                } elseif ($data->status == OrderStatus::IN_WORK) {
                    return ['style' => 'background-color:#bae1ff'];
                } else {
                    return ['style' => 'background-color:#baffc9'];
                }
            },
            'columns' => [
                'id',
                [
                    'attribute' => 'user_id',
                    'filter' => $users,
                    'value' => function ($data) use ($users) {
                        /* @var $data Orders */
                        return $users[$data->user_id] ?? 'не задан';
                    }
                ],
                [
                    'attribute' => 'importer_id',
                    'filter' => $importers,
                    'value' => function ($data) use ($importers) {
                        /* @var $data Orders */
                        return $importers[$data->importer_id] ?? 'не задан';
                    }
                ],
                [
                    'attribute' => 'load_date',
                    'format' => ['date', 'php:d.m.Y'],
                ],
                [
                    'attribute' => 'status',
                    'filter' => OrderStatus::listData(),
                    'value' => function ($data) {
                        /* @var $data Orders */
                        return OrderStatus::getLabel($data->status);

                    }
                ],
                'count_position',
                [
                    'class' => 'kartik\grid\ActionColumn',
                    'header' => '&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspДействия&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp',
                    'dropdown' => false,
                    'vAlign' => 'middle',
                    'urlCreator' => function ($action, $model, $key, $index) {
                        return Url::to([$action, 'id' => $key]);
                    },
                    'template' => '{export} &nbsp {view} &nbsp {upload-input} &nbsp {upload-output1} &nbsp {upload-output2}',
                    'buttons' => [
                        'export' => function ($url, $data, $key) {
                            return ($data->status == OrderStatus::IN_WORK || $data->status == OrderStatus::LOADED)  ? Html::a('<i class="glyphicon glyphicon-eject"></i>', ['orders/export/','id'=> $key]  ) : '';
                        },

                        'upload-input' => function ($url, $data, $key) {
                            return Html::a('<i class="glyphicon glyphicon-floppy-save"></i>', 'upload/' . $key . '/' . $data->xls1);
                        },
                        'upload-output1' => function ($url, $data, $key) {
                            return Html::a('<i class="glyphicon glyphicon-floppy-save"></i>', 'upload/' . $key . '/' . $data->xls2);
                        },
                        'upload-output2' => function ($url, $data, $key) {
                            return Html::a('<i class="glyphicon glyphicon-floppy-save"></i>', 'upload/' . $key . '/' . $data->xls3);
                        },
                    ],
                    'viewOptions' => ['role' => 'modal-remote', 'title' => 'View', 'data-toggle' => 'tooltip'],
                ],
            ],
            'toolbar' => [
                [
                    'content' =>

                        Html::a('<i class="glyphicon glyphicon-repeat"></i>', [''],
                            ['data-pjax' => 1, 'class' => 'btn btn-default', 'title' => 'Reset Grid']) .
                        '{toggleData}' .
                        '{export}'
                ],
            ],
            'striped' => true,
            'condensed' => true,
            'responsive' => true,
            'panel' => [
                'type' => 'primary',
                'heading' => '<i class="glyphicon glyphicon-list"></i> Заказы',
                'before' => '',
                'after' => false,
            ]
        ]) ?>
    </div>

</div>
