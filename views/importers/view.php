<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Importers */
?>
<div class="importers-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'inn',
            'gs1_login',
            'gs1_password',
        ],
    ]) ?>

</div>
