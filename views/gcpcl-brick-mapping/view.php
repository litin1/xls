<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\GcpclBrickMapping */
?>
<div class="gcpcl-brick-mapping-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'mdo_type',
            'valdictpar',
            'gpc',
            'gpc_valdictpar',
        ],
    ]) ?>

</div>
