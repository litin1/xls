<?php


/* @var $this yii\web\View */
/* @var $model app\models\GcpclBrickMapping */

?>
<div class="gcpcl-brick-mapping-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
