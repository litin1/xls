<?php

/* @var $this yii\web\View */
/* @var $model app\models\GcpclBrickMapping */
?>
<div class="gcpcl-brick-mapping-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
