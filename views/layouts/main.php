<?php

/* @var $this \yii\web\View */

/* @var $content string */

use app\assets\AppAsset;
use app\models\enums\DictionaryType;
use app\models\enums\UserRole;
use app\widgets\Alert;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;

if (!Yii::$app->user->isGuest) {
    $role = Yii::$app->user->identity->role;
} else {
    $role = null;
}
AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => Yii::$app->name,
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
            [
                'label' => 'Загрузка из xls',
                'url' => ['/site/index'],
            ],
            [
                'label' => 'Заказы',
                'url' => ['/orders/index'],
                'visible' => $role == UserRole::ADMIN,
            ],
            [
                'label' => 'Обмен файлами',
                'url' => ['/transport'],
                'visible' => $role == UserRole::ADMIN,
            ],
            [
                'label' => 'Настройки',
                'url' => ['/settings/index'],
                'visible' => $role == UserRole::ADMIN,
            ],
            [
                'label' => 'Стравочники',
                'url' => '#',
                'visible' => $role == UserRole::ADMIN,
                'items' => [
                    ['label' => 'Пользователи', 'url' => ['/user/index']],
                    ['label' => 'GCPCL маппинг', 'url' => ['/gcpcl-brick-mapping/index']],
                    ['label' => 'Импортеры', 'url' => ['/importers/index']],
                    ['label' => 'Страны', 'url' => ['/dictionaries/index', 'type' => DictionaryType::COUNTRY]],
                    ['label' => 'Тип обуви', 'url' => ['/dictionaries/index', 'type' => DictionaryType::SHOES_TYPE]],
                    [
                        'label' => 'Материал верха',
                        'url' => ['/dictionaries/index', 'type' => DictionaryType::TOP_MATERIAL]
                    ],
                    [
                        'label' => 'Материал подкладки',
                        'url' => ['/dictionaries/index', 'type' => DictionaryType::SUBSTRATE_MATERIAL]
                    ],
                    [
                        'label' => 'Материал низа',
                        'url' => ['/dictionaries/index', 'type' => DictionaryType::BOTTOM_MATERIAL]
                    ],
                    ['label' => 'Размер обуви', 'url' => ['/dictionaries/index', 'type' => DictionaryType::DIMENSION]],
                ],
            ],
            Yii::$app->user->isGuest ? (
            ['label' => 'Вход', 'url' => ['/site/login']]
            ) : (
                '<li>'
                . Html::beginForm(['/site/logout'], 'post')
                . Html::submitButton(
                    'Выход (' . Yii::$app->user->identity->username . ')',
                    ['class' => 'btn btn-link logout']
                )
                . Html::endForm()
                . '</li>'
            )
        ],
    ]);
    NavBar::end();
    ?>

    <div class="container-fluid">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; My Company <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
